//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

use core::slice;
use crate::interrupts::{self,InterruptTable};
use crate::memory::PageTable as PT;

// This gets passed in via assembly, and contains some important info
// that gets captured during startup
#[repr(C)]
pub struct RawBootInfo {
    pub magic: u32,
    pub multiboot: u32,
    pub interrupts: *mut InterruptTable,
    pub pml4: *mut PT
}
// This is a safe version of RawBootInfo, we can turn the unsafe version
// into this and then safely parse all of the data
pub struct BootInfo {
    pub magic: u32,
    pub multiboot_size: usize,
    //TODO: Is this enough tags?
    pub multiboot: [Tag; 64],
    interrupts: &'static mut InterruptTable,
    pub pml4: *mut PT
}
// This struct holds a single entry in the list of physical memory chunks
// that is contained in the bootinfo
#[derive(Copy,Clone,Debug)]
#[repr(C)]
pub struct MemoryMapEntry {
    pub base_addr: u64,
    pub length: u64,
    pub ty: u32,
    reserved: u32
}
// This enum holds all of the multiboot tags that we care about. There's
// a lot more tags that could be added but these are the ones we need to
// get the system up off the ground.
#[derive(Copy,Clone,Debug)]
pub enum Tag {
    Memory{mem_lower: u32, mem_upper: u32},
    Null,
    MemoryMap{version: u32, entries: &'static [MemoryMapEntry]},
}
impl BootInfo {
    // This function turns our RawBootInfo into a BootInfo
    pub fn new(raw: *mut RawBootInfo) -> BootInfo{ 
        let bootinfo = raw;
        let mut multiboot_size = 0;
        let mut multiboot = [Tag::Null; 64];
        unsafe {
            // This chunk of code parses all of the tags in the bootinfo.
            // It's pretty lovecraftian, the way the bootinfo is set up
            // forces you to use pointer arithmetic.

            //Tags are 8-byte aligned, so force alignment
            if (*bootinfo).multiboot & 0x7 != 0 {
                (*bootinfo).multiboot = ((*bootinfo).multiboot & (!0x7)) + 8
            }
            let mut cur = (*bootinfo).multiboot as *mut u32;
            let mut size = (*cur as i32) - 8;
            cur = cur.offset(2); //Increment pointer by 8 bytes
            while size > 0 {
                let tag_type = *cur;
                let tag_size = *cur.offset(1);
                multiboot[multiboot_size] = match tag_type {
                    4 => Tag::Memory{
                        mem_lower: *cur.offset(2),
                        mem_upper: *cur.offset(3),
                    },
                    6 => Tag::MemoryMap {
                        version: *cur.offset(3),
                        entries: slice::from_raw_parts(cur.offset(4) as *const MemoryMapEntry, ((tag_size-16)/ *cur.offset(2)) as usize),
                    },
                    _ => Tag::Null
                };
                //keep address 8-byte aligned
                cur = (cur as *mut u8).offset(if tag_size % 8 == 0 {
                    //SO MUCH TYPE CONVERSION MY EYES
                    tag_size as isize
                } else {
                    size -= 8 - (tag_size % 8) as i32;
                    ((tag_size & (!0x7)) + 8) as isize
                }) as *mut u32;
                multiboot_size+=1;
                size -= tag_size as i32;
            }
        }
        unsafe {
            BootInfo {
                magic: (*bootinfo).magic,
                //multiboot: (*bootinfo).multiboot,
                multiboot_size,
                multiboot,
                interrupts: &mut (*(*bootinfo).interrupts),
                pml4: (*bootinfo).pml4
            }
        }
    }
    // This function checks whether the magic value that we got
    // from bootinfo is correct
    pub fn bad_magic(&self) -> bool {
        self.magic != 0x36d76289
    }
    // This function takes the interrupt table that was passed in
    // to our BootInfo struct and uses it to initialize the system
    // interrupts
    pub fn initialize_interrupts(&mut self) {
        interrupts::initialize(&mut self.interrupts);
    }
    // This function returns the first Memory tag present in the bootinfo.
    // The memory tag only contains a small portion of the memory on the
    // system.
    pub fn get_basic_memory(&self) -> Option<(u32,u32)> {
        for i in 0 .. 64 {
            match self.multiboot[i] {
                Tag::Memory{mem_lower: lower, mem_upper: upper} => {
                    return Some((lower,upper ));
                },
                _ => {}
            }
        }
        None

    }
    // This function returns the first MemoryMap tag present in 
    // the bootinfo. This tag contains most of the physical memory
    // present on the system
    pub fn get_memory_map(&self) -> Option<(u32,&'static[MemoryMapEntry])> {
        for i in 0 .. 64 {
            match self.multiboot[i] {
                Tag::MemoryMap{ version, entries} => {
                    return Some((version,entries));
                },
                _ => {}
            }
        }
        None

    }
}
