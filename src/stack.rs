//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

// This file contains an implementation of an array-backed stack. We use
// this to avoid using the heap.
use core::cmp;
use core::default::Default;
use core::fmt::{self,Debug};

//How big our static stack buffers should be
pub const STATIC_STACK_SIZE: usize = 256;

pub struct StaticStack<T: Sized > {
    buffer: [T; STATIC_STACK_SIZE],
    counter: usize
}
// This lets us print the contents of a stack for easier debugging. We
// also need it to get tests to work
impl<T: Default + Sized + Debug> Debug for StaticStack<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "StaticStack {{ buffer: [ ")?;
        for i in 0 .. STATIC_STACK_SIZE {
            write!(f,"{:?}, ",self.buffer[i])?;
        }
        write!(f, "], counter: {:?} }}", self.counter)
    }
}

impl<T: Default + Sized + Copy> StaticStack<T> {
    // This contructor works for any type that has a default value.
    // Unfortunately we can't make it const, as default() is not a const
    // function
    pub fn new() -> StaticStack<T> {
        StaticStack { buffer: [Default::default(); STATIC_STACK_SIZE], counter: 0}
    }
}
impl<T: Sized + Copy> StaticStack<T> {
    // This constructor takes a value and gives us a stack full of copies
    // of that value. It is a const function so we can make static global
    // stacks.
    pub const fn new_const(value: T) -> StaticStack<T> {
        StaticStack { buffer: [value; STATIC_STACK_SIZE], counter: 0}
    }
    // This pushes an element onto the stack
    pub fn push(&mut self, data: T) {
        //Check to make sure we don't overflow the stack
        if self.counter != STATIC_STACK_SIZE {
            self.buffer[self.counter] = data;
            //Cap the counter at the max stack size
            self.counter += 1;
        }
    }
    // This pops an element off of the stack
    pub fn pop(&mut self) -> Option<T> {
        //Cap the counter at 0
        self.counter=cmp::max(0,self.counter.checked_sub(1)?);
        //Clone to move out of slice
        self.buffer.get(self.counter).cloned()
    }
    pub fn as_slice(&self) -> &[T] {
        &self.buffer[0 .. self.counter]
    }
    pub fn as_mut_slice(&mut self) -> &mut [T] {
        &mut self.buffer[0 .. self.counter]
    }
}

#[cfg(test)]
mod test {
    use std;
    use proptest::prelude::*;
    use super::StaticStack;
    use crate::stack::STATIC_STACK_SIZE;
    use std::vec::Vec;
    fn vec_and_index() -> BoxedStrategy<(Vec<u8>, usize)> {
        prop::collection::vec(prop::num::u8::ANY, 1..=STATIC_STACK_SIZE)
            .prop_flat_map(|vec| {
                let len = vec.len();
                (Just(vec), 0..len)
            }).boxed()
    }
    proptest!{
        #[test]
        fn push_pop_many(
            (ref mut bytes, _) in vec_and_index()) {

            let mut stack: StaticStack<u8> = StaticStack::new();

            for i in bytes.iter() {
                stack.push(*i);
            }

            assert_eq!(stack.pop(),bytes.pop())
            
        }
        #[should_panic]
        #[test]
        fn push_pop_overflow(
            ref mut bytes in prop::collection::vec(
                prop::num::u32::ANY, 
                STATIC_STACK_SIZE+1..STATIC_STACK_SIZE*2)
            ) {

            prop_assume!(bytes[bytes.len()-1] != bytes[STATIC_STACK_SIZE-1]);

            let mut stack: StaticStack<u32> = StaticStack::new();

            for i in bytes.iter() {
                stack.push(*i);
            }

            assert_eq!(stack.pop(),bytes.pop())
            
        }

        #[test]
        fn slice_subtraction(
            (ref mut bytes, subtract) in vec_and_index(),
            ) {

            let mut stack: StaticStack<u8> = StaticStack::new();

            for i in bytes.iter() {
                stack.push(*i);
            }
            for _ in 0..subtract {
                let _ = stack.pop();
            }

            assert_eq!(*stack.as_slice(),bytes[0..(bytes.len() - subtract)])
        }
        #[test]
        fn push_pop_underflow(
            (ref mut bytes, _) in vec_and_index(), 
            pops in 0 .. STATIC_STACK_SIZE * 20) {

            prop_assume!(pops >= bytes.len());

            let mut stack: StaticStack<u8> = StaticStack::new();

            for i in bytes.iter() {
                stack.push(*i);
            }

            for _ in 0..pops {
                let _ = stack.pop();
            }

            assert_eq!(stack.pop(),None)
        }

    }

}
