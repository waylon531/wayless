//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

// These definitions are needed to make bindgen happy, to let use use
// stuff from seL4.
pub mod os {
    #[allow(non_camel_case_types)]
    pub mod raw {
        pub type c_void = core::ffi::c_void;
        pub type c_char = i8;
        pub type c_short = i16;
        pub type c_int = i32;
        pub type c_long = i64;
        pub type c_longlong = i64;
        pub type c_uchar = u8;
        pub type c_ushort = u16;
        pub type c_uint = u32;
        pub type c_ulong = u64;
        pub type c_ulonglong = u64;
    }
}
