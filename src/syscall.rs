//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

use x86::msr;
use crate::thread::{Context,self};
use crate::error::Error;

extern "C" {
    fn syscall_entry();
    fn return_to_thread(cr3: u64) -> !;
    static mut segments: [u16; 6];
}


//// This macro takes a list of syscalls and ties them to the corresponding
//// seL4 syscalls, ensuring that our syscalls have the same numbering
//// as seL4.
//macro_rules! syscalls {
//    ($($syscall:ident),+ $(,)*) => {
//        //We use this mashup macro so we can concatenate stuff
//        //concat_idents can't be used in pattern position
//        mashup! {
//            $(
//                sys["sys" $syscall] = syscall_ $syscall;
//             )+
//        }
//
//        #[derive(Debug,PartialEq)]
//        pub enum Syscall {
//            //Assign seL4's syscall number to each variant of Syscall
//            $($syscall = sys!(crate::sel4::"sys"$syscall) as isize),+,
//        }
//        impl Syscall {
//            //This function lets us convert from syscall number
//            //to a Syscall
//            pub fn from_i32(syscall_num: i32) -> Option<Syscall> {
//                use self::Syscall::*;
//
//                match syscall_num {
//                    //Turn a syscall number from seL4 into a Syscall
//                    $(
//                        sys!(crate::sel4::"sys"$syscall) => 
//                        Some($syscall)),+,
//                    _ => None
//                }
//            }
//        }
//    }
//}
//
//syscalls!{
//    SysSend,
//    SysNBSend,
//    SysCall,
//    SysRecv,
//    SysReply,
//    SysReplyRecv,
//    SysNBRecv,
//    SysYield,
//}
//
//Import everything from the api
use wayless_api::syscall::*;
#[cfg(test)]
mod test {
    use std;
    use wayless_api::sel4;
    use super::Syscall;
    use proptest::prelude::*;

    proptest!{
        //Check to see if syscall numbers that are in range give a valid
        //syscall
        #[test]
        fn in_range_test(syscall_num in sel4::SYSCALL_MIN ..= sel4::SYSCALL_MAX) {
            Syscall::from_i32(syscall_num).unwrap()
        }
        //Check to see if syscall is valid, if it's in range or not
        #[test]
        fn matches_test(syscall_num in any::<i32>()) {
            let syscall = Syscall::from_i32(syscall_num);
            if syscall_num >= sel4::SYSCALL_MIN && syscall_num <= sel4::SYSCALL_MAX {
                syscall.unwrap();
            } else {
                assert_eq!(syscall,None)
            }
        }
    }
}

// This sets up our syscall handler. It sticks the address of the
// handler into the right register, along with the needed entries
// into the GDT.
pub fn init() {
    //Set up the correct indices into the GDT
    let sysret_ss = 0x10;
    let syscall_cs = 0x08;
    let star: u64 = sysret_ss << 48 | syscall_cs << 32;
    
    //Set the MSRs to point to the correct descriptors
    //and syscall handler function
    unsafe {
        msr::wrmsr(msr::IA32_STAR,star);
        println!("SYSCALL_ENTRY: 0x{:x}",syscall_entry as u64);
        msr::wrmsr(msr::IA32_LSTAR,syscall_entry as u64 );

        //Finally, enable syscalls
        let mut efer = msr::rdmsr(msr::IA32_EFER);
        efer |= 1;
        msr::wrmsr(msr::IA32_EFER,efer);
    }
}

//NOTE: I'm not sure whether syscall should be a i32 or i64
#[no_mangle]
pub fn handle_raw_syscall(
        cptr: u64, 
        message_info: u64,
        syscall: i32, 
        //Accessing these should be safe because we've already
        //acquired the syscall lock
        regs: *mut Context,
        user_cr3: *mut u64) {

    let regs = unsafe {&mut *regs};
    match Syscall::from_i32(syscall) {
        Some(Syscall::SysSend) => {
            //TODO: maybe handle this error
            let _result = handle_send(false,true,cptr,message_info,regs);
            //Maybe TODO:
            //seL4 does more stuff here
        },
        Some(Syscall::SysNBSend) => {
            //TODO: maybe handle this error
            let _result = handle_send(false,false,cptr,message_info,regs);
        },
        Some(Syscall::SysCall) => {
            //TODO: definitely handle this one
            let _result = handle_send(true,true,cptr,message_info,regs);
        },
        Some(Syscall::SysRecv) => {
            handle_recv(true);
        },
        Some(Syscall::SysReply) => {
            handle_reply(message_info,regs);
        }
        Some(Syscall::SysReplyRecv) => {
            handle_reply(message_info,regs);
            handle_recv(true);
        },
        Some(Syscall::SysNBRecv) => {
            handle_recv(false);
        },
        Some(Syscall::SysYield) => {
            handle_yield(regs);
        },
        None => handle_unknown_syscall(syscall)
    }
    //Resume our thread
    let thread = thread::current_thread().unwrap();
    unsafe {*(user_cr3) = thread.get_vspace();}
}


fn handle_unknown_syscall(_syscall: i32) {
    //TODO: this shouldn't crash the kernel like this
    unimplemented!()
}


//We need an exception type still
//that maps to seL4 enums
fn handle_send(is_call: bool, _is_blocking: bool, cptr: u64,message_info: u64, user_context: &mut Context) -> Result<(),Error> {
    //This unwrap is (probably) safe 
    //because there needs to be a thread to run the syscall
    let thread = thread::current_thread().unwrap();
    //let info = thread.get_info();
    //let cptr = thread.get_register(Register::CapRegister);
    if let Some(root_cap) = thread.get_root() {
        if let Ok(expected_cap) = root_cap.get_cap(cptr,64){
            let context = user_context.regs;
            let mut register_message = [context[12],context[10],context[11],context[17]];
            let label = message_info >> 12;
            //Get 3 bits of unwrapped caps
            let _caps_unwrapped = message_info >> 9 & 0x7;
            //Get 2 bits of extra caps
            let _extra_caps = message_info >> 7 & 0x3;
            let length = message_info & ((1<<7)-1);
            //TODO: handle longer messages
            expected_cap.send(label,length,&mut register_message); 

            // If this was a call write the message back
            if is_call {
                //TODO: set up reply cap
                user_context.regs[12] = register_message[0];
                user_context.regs[10] = register_message[1];
                user_context.regs[11] = register_message[2];
                user_context.regs[17] = register_message[3];
            }
            //TODO: write message info back to thread
        }

    }
    //TODO: not always okay
    Ok(())

    //(cap, slot) = match thread.lookup_cap_and_slot(cptr) {
    //    Ok(ok) => ok,
    //    Err(e) => {
    //        //userError("Invocation of invalid cap #%lu.", cptr);
    //        //current_fault = seL4_Fault_CapFault_new(cptr, false);
    //        if (is_blocking) {
    //            thread.handle_fault(); //Should I pass in the error?
    //            //Or current_fault?
    //        }
    //        return Err(e)

    //    }
    //}


}
fn handle_reply(message_info: u64, user_context: &mut Context) -> Result<(),Error> {
    //This unwrap is (probably) safe 
    //because there needs to be a thread to run the syscall
    let mut thread = thread::current_thread().unwrap();

    if let Some(reply) = thread.get_reply_cap() {
        //TODO: this is redundant code
        let context = user_context.regs;
        let mut register_message = [context[12],context[10],context[11],context[17]];
        let label = message_info >> 12;
        //Get 3 bits of unwrapped caps
        let _caps_unwrapped = message_info >> 9 & 0x7;
        //Get 2 bits of extra caps
        let _extra_caps = message_info >> 7 & 0x3;
        let length = message_info & ((1<<7)-1);
        //TODO: handle longer messages
        reply.send(label,length,&mut register_message); 

    }
    //We need to update the thread as we removed the reply cap from it
    thread::replace_thread(thread);
    Ok(())
}
fn handle_yield(context: *mut Context) -> !{ 
    //These segments are always correct as we are always in kernel mode
    //in a syscall
    //let mut segments = [0x8,0x10,0x10,0x10,0x10,0x10];
    let cr3 = unsafe {thread::switch_thread(context, &mut segments)};
    //If we need to return to usermode just go for it
    unsafe {return_to_thread(cr3);}
}
// I don't need to implement this until I have message passing
fn handle_recv(_is_blocking: bool) {
    unimplemented!();
}
