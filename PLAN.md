# Major Steps for Implementation

The goal here is to implement a useful subset of seL4 with a slightly 
simplified implementation of capability addressing. The capability
system in wayless will work much more like a filesystem path than a 
virtual memory address, as it is implemented in seL4.

## Unit Tests
Unit tests, while not essential, will make the kernel significantly easier 
to debug. They will also prevent regressions and catch some easy bugs. 
These should be easy to implement as I'm coding stuff.

## Capabilities
Capabilities control most access to the kernel, including message passing
and thread control.
There are a few types of capabilities:
* Capability Directories
* Message Passing Endoints
* IO Ports
* Thread Control Capabilities
* Page Tables and Pages
* Raw Memory Chunks
* Links

These capability types map very similarly onto sel4 capabilities. 
In addition, different types of capabilities will require different 
structures in the kernel.
For example, thread control capabilities will need context switching +
other thread related stuff implemented in the kernel.
This will let me implement portions of the kernel at a time, and will
mean I can scrap certain classes of capabilities if worse comes to worse.

## Drivers

Once capabilities are implemented and the kernel is able to start up a 
usermode process, drivers will start to be implemented.
This will likely be simple things, as the goal is to create a tech demo
at the end of the term.
These might be things such as a PS/2 keyboard driver, a serial port driver,
and potentially a text-mode graphics driver.

## Tech Demo

The goal for the end of the term is to have a tech demo that shows off the
features of the operating system and looks pretty for all the non-tech 
inclined people I'll be presenting to. This will likely make use of
most of the implemented drivers.
