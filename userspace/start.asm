extern _main
; This is the default location for the serial port
%assign SERIAL 0x3f8

%macro outs 1
    %strlen charnum %1
    %assign i 1


    ; Output all the characters in the string
    %rep charnum
        %substr x %1 i
        outb SERIAL, x
        %assign i i+1
    %endrep

    ;output a newline
    outb SERIAL, 10
%endmacro

;%macro outb 1
;    mov rdx, -1
;    ; set up the right label
;    mov rsi, 46 << 12
;    ; the io port is at 0 
;    mov rdi, 0 
;    mov r10, SERIAL
;    mov r8, %1
;    syscall
;%endmacro

;%define outb(x) outb SERIAL x

%macro outb 2
    mov rdx, -1
    ; set up the right label
    mov rsi, 46 << 12
    ; the io port is at 0 
    mov rdi, 0 
    mov r10, %1
    mov r8, %2
    syscall
%endmacro

%macro inb 1
    ; Call is -1
    mov rdx, -1
    ; set up the right label
    mov rsi, 43 << 12
    ; the io port is at 0 
    mov rdi, 0 
    ; and the port we want is the serial port,
    ; which is at 0x3F8
    mov r10, %1
    syscall
    ; the result will be in r10
%endmacro

%macro print_prompt 0
    outb SERIAL, " "
    outb SERIAL, "$"
    outb SERIAL, " "
%endmacro

section .text

[bits 64]
global _start
_start:
    ; Initialize serial port

    outb (SERIAL + 1), 0x00;    // Disable all interrupts
    outb (SERIAL + 3), 0x80;    // Enable DLAB (set baud rate divisor)
    outb (SERIAL + 0), 0x03;    // Set divisor to 3 (lo byte) 38400 baud
    outb (SERIAL + 1), 0x00;    //                  (hi byte)
    outb (SERIAL + 3), 0x03;    // 8 bits, no parity, one stop bit
    outb (SERIAL + 2), 0xC7;    // Enable FIFO, clear them, with 14-byte threshold
    outb (SERIAL + 4), 0x0B;    // IRQs enabled, RTS/DSR set

    outs ""
    outs "__        __          _               "
    outs "\ \      / /_ _ _   _| | ___  ___ ___ "
    outs " \ \ /\ / / _` | | | | |/ _ \/ __/ __|"
    outs "  \ V  V / (_| | |_| | |  __/\__ \__ \"
    outs "   \_/\_/ \__,_|\__, |_|\___||___/___/"
    outs "                |___/                 "
    outs ""
                                                      
    ; Set up a statically allocated stack so Rust code actually works
    mov rsp, stack_top
    ; Jump to the main function of our Rust userspace code
    call _main
    ;print_prompt

    ;; Buffer counter
    ;xor r14,r14
    ;.print_loop:
    ;    ; Read the line status register
    ;    ; until there's a character ready
    ;    .get_character:
    ;        inb SERIAL+5
    ;        and r10, 1
    ;        jz .get_character

    ;    ; Then read and print it
    ;    inb SERIAL
    ;    mov r15b, r10b
    ;    ; Turn carriage returns into newlines
    ;    cmp r15b, 13
    ;    je .newline
    ;    ; Handle backspaces
    ;    cmp r15b, 0x7f
    ;    je .backspace
    ;    ; Handle exit via Ctrl+D
    ;    cmp r15b, 0x4
    ;    je exit
    ;    .default:
    ;        ; Print the received character
    ;        outb SERIAL, r15
    ;        ; And stick it in our buffer
    ;        mov [buffer+r14*1], r15b
    ;        inc r14
    ;        jmp .print_loop
    ;        ; Or print a newline if it's a carriage return
    ;    .newline:
    ;        outb SERIAL, 10
    ;        ; Print out our array
    ;        xor r12,r12
    ;        ; Exit early if r14 is zero
    ;        cmp r14, 0
    ;        jz .end
    ;        .printloop:
    ;            ; Get the character from the buffer and print it
    ;            mov r13b, [buffer+r12*1]
    ;            outb SERIAL, r13
    ;            ; Then move on to the next character
    ;            inc r12
    ;            cmp r12,r14
    ;            jl .printloop

    ;        ; Reset our buffer index to 0
    ;        xor r14,r14
    ;            
    ;        .end:
    ;            outb SERIAL, 10
    ;            print_prompt
    ;            jmp .print_loop

    ;    .backspace:
    ;        dec r14
    ;        ; If it underflowed then set r14 to zero
    ;        jge .else
    ;            xor r14,r14
    ;            jmp .print_loop
    ;        .else:
    ;            ; Clear the character
    ;            outb SERIAL, 8
    ;            outb SERIAL, ' '
    ;            ; Then go back again
    ;            outb SERIAL, 8
    ;            jmp .print_loop

    ;; Shut wayless down via the debug-exit port
    ;; And if that doesn't work just loop forever
    exit:
        outb 0xF4, 0
        jmp exit


section .bss
    ; Reserve a stack so we can run rust code
    alignb 8
    resb 4096
    stack_top:
    ; Reserve a 1000 byte buffer for string stuff
    buffer: resb 1000
; vim: ft=nasm
